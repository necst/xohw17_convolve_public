#include "convolve_hls.h"

/*
 * Matrix([[K[0, 0],
 *                      0.5*K[0, 0] + 0.5*K[0, 1] + 0.5*K[0, 2],
 *                      0.5*K[0, 0] - 0.5*K[0, 1] + 0.5*K[0, 2],
 *                      K[0, 2]],
 *
           [0.5*K[0, 0] + 0.5*K[1, 0] + 0.5*K[2, 0],
                        0.5*(0.5*K[0, 0] + 0.5*K[0, 1] + 0.5*K[0, 2]) + 0.5*(0.5*K[1, 0] + 0.5*K[1, 1] + 0.5*K[1, 2]) + 0.5*(0.5*K[2, 0] + 0.5*K[2, 1] + 0.5*K[2, 2]),
                        0.5*(0.5*K[0, 0] - 0.5*K[0, 1] + 0.5*K[0, 2]) + 0.5*(0.5*K[1, 0] - 0.5*K[1, 1] + 0.5*K[1, 2]) + 0.5*(0.5*K[2, 0] - 0.5*K[2, 1] + 0.5*K[2, 2]),
                        0.5*K[0, 2] + 0.5*K[1, 2] + 0.5*K[2, 2]],

           [0.5*K[0, 0] - 0.5*K[1, 0] + 0.5*K[2, 0],
                        0.5*(0.5*K[0, 0] + 0.5*K[0, 1] + 0.5*K[0, 2]) - 0.5*(0.5*K[1, 0] + 0.5*K[1, 1] + 0.5*K[1, 2]) + 0.5*(0.5*K[2, 0] + 0.5*K[2, 1] + 0.5*K[2, 2]),
                        0.5*(0.5*K[0, 0] - 0.5*K[0, 1] + 0.5*K[0, 2]) - 0.5*(0.5*K[1, 0] - 0.5*K[1, 1] + 0.5*K[1, 2]) + 0.5*(0.5*K[2, 0] - 0.5*K[2, 1] + 0.5*K[2, 2]),
                        0.5*K[0, 2] - 0.5*K[1, 2] + 0.5*K[2, 2]],

           [K[2, 0],
                        0.5*K[2, 0] + 0.5*K[2, 1] + 0.5*K[2, 2],
                        0.5*K[2, 0] - 0.5*K[2, 1] + 0.5*K[2, 2], K[2, 2]]])
 */

void kernel_transform(float input[3][3], float output[4][4]) {

	//first row
	float temp1a = input[0][0] + input[0][2];

	output[0][0] = input[0][0];
	output[0][1] = (temp1a + input[0][1])/2;
	output[0][2] = (temp1a - input[0][1])/2;
	output[0][3] = input[0][2];

	//second row
	float temp2a = input[0][0] + input[2][0];
	float temp2b = input[1][0] + input[1][2];
	float temp2c = input[2][0] + input[2][2];
	float temp2d = (temp2b + input[1][1])/4;
	float temp2e = (temp2c + input[2][1])/2;
	float temp2f = (temp2b - input[1][1])/4;
	float temp2g = (temp2c - input[2][1])/2;
	float temp2h = output[0][1]/2;
	float temp2i = output[0][2]/2;
	float temp2j = input[0][2] + input[2][2];
	float temp2k = temp2e/2;
	float temp2l = temp2g/2;

	output[1][0] = (temp2a + input[1][0])/2;
	output[1][1] = temp2h + temp2d + temp2k;
	output[1][2] = temp2i + temp2f + temp2l;
	output[1][3] = (temp2j + input[1][2])/2;

	//third row
	output[2][0] = (temp2a - input[1][0])/2;
	output[2][1] = temp2h - temp2d + temp2k;
	output[2][2] = temp2i - temp2f + temp2l;
	output[2][3] = (temp2j - input[1][2])/2;

	//fourth row
	float temp4a = input[2][0] + input[2][2];

	output[3][0] = input[2][0];
	output[3][1] = temp2e;
	output[3][2] = temp2g;
	output[3][3] = input[2][2];
}

/*
 * Matrix([[-(I[2, 0] - I[2, 2]) + I[0, 0] - I[0, 2],    -(I[2, 1] + I[2, 2]) + I[0, 1] + I[0, 2],   -(-I[2, 1] + I[2, 2]) - I[0, 1] + I[0, 2],      -(I[2, 1] - I[2, 3]) + I[0, 1] - I[0, 3]],
 *         [  I[1, 0] - I[1, 2] + I[2, 0] - I[2, 2],       I[1, 1] + I[1, 2] + I[2, 1] + I[2, 2],    -I[1, 1] + I[1, 2] - I[2, 1] + I[2, 2],           I[1, 1] - I[1, 3] + I[2, 1] - I[2, 3]],
 *         [-(I[1, 0] - I[1, 2]) + I[2, 0] - I[2, 2],    -(I[1, 1] + I[1, 2]) + I[2, 1] + I[2, 2],   -(-I[1, 1] + I[1, 2]) - I[2, 1] + I[2, 2],      -(I[1, 1] - I[1, 3]) + I[2, 1] - I[2, 3]],
 *         [-(I[3, 0] - I[3, 2]) + I[1, 0] - I[1, 2],    -(I[3, 1] + I[3, 2]) + I[1, 1] + I[1, 2],   -(-I[3, 1] + I[3, 2]) - I[1, 1] + I[1, 2],      -(I[3, 1] - I[3, 3]) + I[1, 1] - I[1, 3]]])
 */

void input_transform(float input[4][4], float output[4][4]) {

	//first row
	float temp1a = input[2][0] - input[2][2];
	float temp1b = input[2][1] + input[2][2];
	float temp1c = - input[2][1] + input[2][2];
	float temp1d = input[2][1] - input[2][3];

	output[0][0] = - temp1a + input[0][0] - input[0][2];
	output[0][1] = - temp1b + input[0][1] + input[0][2];
	output[0][2] = - temp1c - input[0][1] + input[0][2];
	output[0][3] = - temp1d + input[0][1] - input[0][3];

	//second row
	float temp2a = input[1][0] - input[1][2];
	float temp2b = input[1][1] + input[1][2];
	float temp2c = - input[1][1] + input[1][2];
	float temp2d = input[1][1] - input[1][3];

	output[1][0] = temp2a + temp1a;
	output[1][1] = temp2b + temp1b;
	output[1][2] = temp2c + temp1c;
	output[1][3] = temp2d + temp1d;

	//third row
	output[2][0] = - temp2a + temp1a;
	output[2][1] = - temp2b + temp1b;
	output[2][2] = - temp2c + temp1c;
	output[2][3] = - temp2d + temp1d;

	//fourth row
	output[3][0] = - input[3][0] + input[3][2] + input[1][0] - input[1][2];
	output[3][1] = - input[3][1] - input[3][2] + temp2b;
	output[3][2] = input[3][1] - input[3][2] + temp2c;
	output[3][3] = - input[3][1] + input[3][3] + temp2d;
}

/*
 * Matrix([[O[0, 0] + O[0, 1] + O[0, 2] + O[1, 0] + O[1, 1] + O[1, 2] + O[2, 0] + O[2, 1] + O[2, 2],
 *                                      O[0, 1] - O[0, 2] - O[0, 3] + O[1, 1] - O[1, 2] - O[1, 3] + O[2, 1] - O[2, 2] - O[2, 3]],
 *        [-(O[2, 0] + O[2, 1] + O[2, 2]) - (O[3, 0] + O[3, 1] + O[3, 2]) + O[1, 0] + O[1, 1] + O[1, 2],
 *                                    -(O[2, 1] - O[2, 2] - O[2, 3]) - (O[3, 1] - O[3, 2] - O[3, 3]) + O[1, 1] - O[1, 2] - O[1, 3]]])
 */
void output_transform(float input[4][4], float output[2][2]) {

	//first row
	float temp1a = input[0][1] + input[1][1] + input[2][1];
	float temp1b = input[0][2] + input[1][2];
	float temp1c = input[2][0] + input[2][2];

	output[0][0] = input[0][0] + temp1a + temp1b + input[1][0] + temp1c;
	output[0][1] = temp1a - temp1b - input[0][3] - input[1][3] - input[2][2] - input[2][3];

	//second row
	output[1][0] = - temp1c - input[2][1] - (input[3][0] + input[3][1] + input[3][2]) + input[1][0] + input[1][1] + input[1][2];
	output[1][1] = - (input[2][1] - input[2][2] - input[2][3]) - (input[3][1] - input[3][2] - input[3][3]) + input[1][1] - input[1][2] - input[1][3];
}

void win22(hls::stream<float> &ker_in_s, hls::stream<out_data> &output_s) {
#pragma HLS INTERFACE axis port=ker_in_s
#pragma HLS INTERFACE axis port=output_s
#pragma HLS INTERFACE ap_ctrl_none port=return

	const int in_depth = IN_DEPTH;
	const int out_depth = OUT_DEPTH;

	float input[IN_DEPTH][HLS_INPUT_SIZE][HLS_INPUT_SIZE];
#pragma HLS ARRAY_PARTITION variable=input block factor=in_depth dim=1

	for (int c = 0; c < IN_DEPTH; c++) {
		for (int i = 0; i < HLS_INPUT_SIZE; i++) {
			for (int j = 0; j < HLS_INPUT_SIZE; j++) {
#pragma HLS PIPELINE II=1
				input[c][i][j] = ker_in_s.read();
			}
		}
	}

	//iterating on output layer
	for (int d = 0; d < OUT_DEPTH; d++) {

		float kernel[IN_DEPTH][KERNEL_HEIGHT][KERNEL_WIDTH];
#pragma HLS ARRAY_PARTITION variable=kernel block factor=in_depth dim=1

		float output_buffer_unsum[IN_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE];
#pragma HLS ARRAY_PARTITION variable=output_buffer_unsum block factor=in_depth dim=1

		//iterating on input layer
		for (int c = 0; c < IN_DEPTH; c++) {
#pragma HLS UNROLL

			for (int i = 0; i < KERNEL_HEIGHT; i++) {
				for (int j = 0; j < KERNEL_WIDTH; j++) {
#pragma HLS PIPELINE II=1
					kernel[c][i][j] = ker_in_s.read();
				}
			}

			float kernel_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
			kernel_transform(kernel[c], kernel_trans);

			struct m_dimension input_offset;
			struct m_dimension output_offset;

			input_offset.height = 0;
			output_offset.height = 0;

			input_offset.width = 0;
			output_offset.width = 0;

			while (input_offset.height <= HLS_INPUT_SIZE - HLS_INPUT_BLOCK_HEIGHT) {

				input_offset.width = 0;
				output_offset.width = 0;

				while (input_offset.width <= HLS_INPUT_SIZE - HLS_INPUT_BLOCK_WIDTH) {
#pragma HLS PIPELINE II=1 enable_flush

					//duplicate input block
					float partial_input[HLS_INPUT_BLOCK_HEIGHT][HLS_INPUT_BLOCK_WIDTH];
					for (int m = 0; m < HLS_INPUT_BLOCK_HEIGHT; m++) {
						for (int n = 0; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = input[c][input_offset.height + m][input_offset.width + n];
						}
					}
					float partial_output[HLS_OUTPUT_BLOCK_HEIGHT][HLS_OUTPUT_BLOCK_WIDTH];

					float input_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
					float output_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];

					input_transform(partial_input, input_trans);

					for (int k = 0; k < HLS_TRANSFORMED_BLOCK_HEIGHT; k++) {
						for (int j = 0; j < HLS_TRANSFORMED_BLOCK_WIDTH; j++) {
							output_trans[k][j] = input_trans[k][j] * kernel_trans[k][j];
						}
					}

					output_transform(output_trans, partial_output);

					//copying partial output in output position
					for (int x = 0; x < HLS_OUTPUT_BLOCK_HEIGHT; x++) {
						for (int y = 0; y < HLS_OUTPUT_BLOCK_WIDTH; y++) {
							output_buffer_unsum[c][output_offset.height + x][output_offset.width + y] = partial_output[x][y];
						}
					}

					input_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
					output_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
				}

				while (output_offset.width < HLS_OUTPUT_SIZE) {

					int input_width_diff = HLS_INPUT_SIZE - input_offset.width;
					int output_width_diff = HLS_OUTPUT_SIZE - output_offset.width;

					//duplicate input block
					float partial_input[HLS_INPUT_BLOCK_HEIGHT][HLS_INPUT_BLOCK_WIDTH];
					for (int m = 0; m < HLS_INPUT_BLOCK_HEIGHT; m++) {
						int n;
						for (n = 0; n < input_width_diff; n++) {
							partial_input[m][n] = input[c][input_offset.height + m][input_offset.width + n];
						}
						for (n; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = 0;
						}
					}
					float partial_output[HLS_OUTPUT_BLOCK_HEIGHT][HLS_OUTPUT_BLOCK_WIDTH];

					float input_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
					float output_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];

					input_transform(partial_input, input_trans);

					for (int k = 0; k < HLS_TRANSFORMED_BLOCK_HEIGHT; k++) {
						for (int j = 0; j < HLS_TRANSFORMED_BLOCK_WIDTH; j++) {
							output_trans[k][j] = input_trans[k][j] * kernel_trans[k][j];
						}
					}

					output_transform(output_trans, partial_output);

					//copying partial output in output position
					for (int x = 0; x < HLS_OUTPUT_BLOCK_HEIGHT; x++) {
						for (int y = 0; y < output_width_diff; y++) {
							output_buffer_unsum[c][output_offset.height + x][output_offset.width + y] = partial_output[x][y];
						}
					}

					input_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
					output_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
				}

				input_offset.height += HLS_OUTPUT_BLOCK_HEIGHT;
				output_offset.height += HLS_OUTPUT_BLOCK_HEIGHT;
			}

			while (output_offset.height < HLS_OUTPUT_SIZE) {

				input_offset.width = 0;
				output_offset.width = 0;

				int input_height_diff = HLS_INPUT_SIZE - input_offset.height;
				int output_height_diff = HLS_OUTPUT_SIZE - output_offset.height;

				bottom_left_blocks: while (input_offset.width <= HLS_INPUT_SIZE - HLS_INPUT_BLOCK_WIDTH) {

					//duplicate input block
					float partial_input[HLS_INPUT_BLOCK_HEIGHT][HLS_INPUT_BLOCK_WIDTH];
					int m;
					for (m = 0; m < input_height_diff; m++) {
						for (int n = 0; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = input[c][input_offset.height + m][input_offset.width + n];
						}
					}
					for (m; m < HLS_INPUT_BLOCK_HEIGHT; m++) {
						for (int n = 0; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = 0;
						}
					}
					float partial_output[HLS_OUTPUT_BLOCK_HEIGHT][HLS_OUTPUT_BLOCK_WIDTH];


					float input_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
					float output_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];

					input_transform(partial_input, input_trans);

					for (int k = 0; k < HLS_TRANSFORMED_BLOCK_HEIGHT; k++) {
						for (int j = 0; j < HLS_TRANSFORMED_BLOCK_WIDTH; j++) {
							output_trans[k][j] = input_trans[k][j] * kernel_trans[k][j];
						}
					}

					output_transform(output_trans, partial_output);


					//copying partial output in output position
					for (int x = 0; x < output_height_diff; x++) {
						for (int y = 0; y < HLS_OUTPUT_BLOCK_WIDTH; y++) {
							output_buffer_unsum[c][output_offset.height + x][output_offset.width + y] = partial_output[x][y];
						}
					}

					input_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
					output_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
				}

				while (output_offset.width < HLS_OUTPUT_SIZE) {

					int input_width_diff = HLS_INPUT_SIZE - input_offset.width;
					int output_width_diff = HLS_OUTPUT_SIZE - output_offset.width;

					//duplicate input block
					float partial_input[HLS_INPUT_BLOCK_HEIGHT][HLS_INPUT_BLOCK_WIDTH];
					int m, n;
					for (m = 0; m < input_height_diff; m++) {
						for (n = 0; n < input_width_diff; n++) {
							partial_input[m][n] = input[c][input_offset.height + m][input_offset.width + n];
						}
					}
					for (m; m < HLS_INPUT_BLOCK_HEIGHT; m++) {
						for (n; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = 0;
						}
					}
					float partial_output[HLS_OUTPUT_BLOCK_HEIGHT][HLS_OUTPUT_BLOCK_WIDTH];

					float input_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
					float output_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];

					input_transform(partial_input, input_trans);

					for (int k = 0; k < HLS_TRANSFORMED_BLOCK_HEIGHT; k++) {
						for (int j = 0; j < HLS_TRANSFORMED_BLOCK_WIDTH; j++) {
							output_trans[k][j] = input_trans[k][j] * kernel_trans[k][j];
						}
					}

					output_transform(output_trans, partial_output);

					//copying partial output in output position
					for (int x = 0; x < output_height_diff; x++) {
						for (int y = 0; y < output_width_diff; y++) {
							output_buffer_unsum[c][output_offset.height + x][output_offset.width + y] = partial_output[x][y];
						}
					}

					input_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
					output_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
				}

				input_offset.height += HLS_OUTPUT_BLOCK_HEIGHT;
				output_offset.height += HLS_OUTPUT_BLOCK_HEIGHT;
			}
		}

		float output_buffer[HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE] = {0};

		for (int c = 0; c < IN_DEPTH; c++) {
			for (int x = 0; x < HLS_OUTPUT_SIZE; x++) {
				for (int y = 0; y < HLS_OUTPUT_SIZE; y++) {
#pragma HLS PIPELINE II=1
					output_buffer[x][y] += output_buffer_unsum[c][x][y];
				}
			}
		}
		out_data out_data;
		for (int x = 0; x < HLS_OUTPUT_SIZE; x++) {
			for (int y = 0; y < HLS_OUTPUT_SIZE; y++) {
#pragma HLS PIPELINE II=1
				out_data.data = output_buffer[x][y];
				out_data.last = (x == (HLS_OUTPUT_SIZE - 1) && y == (HLS_OUTPUT_SIZE - 1) && (d == OUT_DEPTH - 1)) ? 1 : 0;
				output_s.write(out_data);
			}
		}
	}
}
