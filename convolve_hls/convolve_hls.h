#ifndef CONVOLVE_HLS_H
#define CONVOLVE_HLS_H

#include <hls_stream.h>
#include <stdio.h>

#define HLS_INPUT_SIZE 64
#define IN_DEPTH 3
#define OUT_DEPTH 64
#define HLS_OUTPUT_SIZE 62
#define HLS_INPUT_BLOCK_WIDTH 4
#define HLS_INPUT_BLOCK_HEIGHT 4
#define HLS_OUTPUT_BLOCK_WIDTH 2
#define HLS_OUTPUT_BLOCK_HEIGHT 2
#define HLS_TRANSFORMED_BLOCK_WIDTH 4
#define HLS_TRANSFORMED_BLOCK_HEIGHT 4
#define KERNEL_WIDTH 3
#define KERNEL_HEIGHT 3

/**
 * @brief describes matrix dimensions
 */
struct m_dimension {
    int height;
    int width;
};

struct out_data{
  float data;
  bool last;
};

void win22(hls::stream<float> &ker_in_s, hls::stream<out_data> &output_s);

#endif // CONVOLVE_HLS_H

