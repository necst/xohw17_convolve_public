#include "convolve_hls.h"
#include <stdio.h>

#define MAX_ERROR 0.001f

void generate_input(float data[IN_DEPTH][HLS_INPUT_SIZE][HLS_INPUT_SIZE]){
	for(size_t c = 0; c < IN_DEPTH; c++) {
		for(size_t i = 0; i < HLS_INPUT_SIZE; i++) {
			for(size_t j = 0; j < HLS_INPUT_SIZE; j++) {
				data[c][i][j] = ((double) rand() / (RAND_MAX));
			}
		}
	}
}

void generate_kernel(float data[OUT_DEPTH][IN_DEPTH][KERNEL_HEIGHT][KERNEL_WIDTH]){
	int e = 0;
	for (size_t d = 0; d < OUT_DEPTH; d++) {
		for(size_t c = 0; c < IN_DEPTH; c++) {
			for(size_t i = 0; i < KERNEL_HEIGHT; i++) {
				for(size_t j = 0; j < KERNEL_WIDTH; j++) {
					data[d][c][i][j] = ((double) rand() / (RAND_MAX));
				}
			}
		}
	}
}

void naive_3D_convolution_output_no_padding(
		const float input[IN_DEPTH][HLS_INPUT_SIZE][HLS_INPUT_SIZE],
		const float kernel[OUT_DEPTH][IN_DEPTH][KERNEL_HEIGHT][KERNEL_WIDTH],
		float output[OUT_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE]) {

	//iterate over output depth
	for (size_t o_depth = 0; o_depth < OUT_DEPTH; o_depth++) {

		size_t i_row_offset = 0;
		size_t i_column_offset = 0;

		//iterate over output rows
		for(size_t o_row = 0; o_row < HLS_OUTPUT_SIZE; o_row++) {

			//iterate over output columns
			for(size_t o_column = 0; o_column < HLS_OUTPUT_SIZE; o_column++) {

				//init output position to 0
				output[o_depth][o_row][o_column] = 0;

				//iterate over input depth
				for(size_t channel = 0; channel < IN_DEPTH; channel++) {

					//iterate over kernel rows
					for (size_t k_row = 0; k_row < KERNEL_HEIGHT; k_row ++) {

						//iterate over kernel columns
						for(size_t k_column = 0; k_column < KERNEL_WIDTH; k_column ++){
							size_t i_row = i_row_offset + k_row;
							size_t i_column = i_column_offset + k_column;

							output[o_depth][o_row][o_column] += input[channel][i_row][i_column] * kernel[o_depth][channel][k_row][k_column];
						}
					}
				}

				if (i_column_offset + KERNEL_WIDTH == HLS_INPUT_SIZE) {
					i_column_offset = 0;
					i_row_offset += 1;
				} else {
					i_column_offset += 1;
				}
			}
		}
	}

}



int main(){

	static float input[IN_DEPTH][HLS_INPUT_SIZE][HLS_INPUT_SIZE];
	static float kernel[OUT_DEPTH][IN_DEPTH][KERNEL_WIDTH][KERNEL_HEIGHT];

	static float naive_output[OUT_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE];
	static float winograd_output[OUT_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE];

	hls::stream<float> ker_in_s;
	hls::stream<out_data> output_s;

	generate_input(input);
	generate_kernel(kernel);

	naive_3D_convolution_output_no_padding(input,kernel,naive_output);

	for (int c = 0; c < IN_DEPTH; c++) {
		for (int i = 0; i < HLS_INPUT_SIZE; i++) {
			for (int j = 0; j < HLS_INPUT_SIZE; j++) {
				ker_in_s.write(input[c][i][j]);
			}
		}
	}

	for (size_t d = 0; d < OUT_DEPTH; d++) {
		for (int c = 0; c < IN_DEPTH; c++) {
			for (int i = 0; i < KERNEL_HEIGHT; i++) {
				for (int j = 0; j < KERNEL_WIDTH; j++) {
					ker_in_s.write(kernel[d][c][i][j]);
				}
			}
		}
	}

	win22(ker_in_s,output_s);

	for (size_t d = 0; d < OUT_DEPTH; d++) {
		for (int i = 0; i < HLS_OUTPUT_SIZE; i++) {
			for (int j = 0; j < HLS_OUTPUT_SIZE; j++) {
				winograd_output[d][i][j] = output_s.read().data;
			}
		}

	}




	printf("Compare HW with SW... \n");

	for (size_t d = 0; d < OUT_DEPTH; d++) {
		for (int i = 0; i < HLS_OUTPUT_SIZE; i++) {
			for (int j = 0; j < HLS_OUTPUT_SIZE; j++) {

				float error = naive_output[d][i][j] - winograd_output[d][i][j];
				if (error < 0) {
					error = - error;
				}
				if (error > MAX_ERROR) {
					printf("Error ");
					return 1;
				}
			}
		}
	}

	printf("Compare OK! \n");

	return 0;
}
