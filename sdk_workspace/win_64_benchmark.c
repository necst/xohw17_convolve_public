#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "xil_printf.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <xil_cache.h>
#include "xil_types.h"
#include "xil_io.h"
#include "xtmrctr.h"
#include "xparameters.h"
#include "xaxidma.h"
#include "platform.h"

#define MAX_ERROR 0.001f
#define TIMER_FACTOR 100000000.0f

#define HLS_INPUT_SIZE 64
#define IN_DEPTH 3
#define OUT_DEPTH 64
#define HLS_OUTPUT_SIZE 62
#define HLS_INPUT_BLOCK_WIDTH 4
#define HLS_INPUT_BLOCK_HEIGHT 4
#define HLS_OUTPUT_BLOCK_WIDTH 2
#define HLS_OUTPUT_BLOCK_HEIGHT 2
#define HLS_TRANSFORMED_BLOCK_WIDTH 4
#define HLS_TRANSFORMED_BLOCK_HEIGHT 4
#define KERNEL_WIDTH 3
#define KERNEL_HEIGHT 3


struct m_dimension {
    int height;
    int width;
};

void generate_input(float data[IN_DEPTH][HLS_INPUT_SIZE][HLS_INPUT_SIZE]){
	size_t c, i, j;
	for(c = 0; c < IN_DEPTH; c++) {
		for(i = 0; i < HLS_INPUT_SIZE; i++) {
			for(j = 0; j < HLS_INPUT_SIZE; j++) {
				data[c][i][j] = ((double) rand() / (RAND_MAX));
			}
		}
	}
}

void generate_kernel(float data[OUT_DEPTH][IN_DEPTH][KERNEL_HEIGHT][KERNEL_WIDTH]){
	size_t d, c, i, j;
	for (d = 0; d < OUT_DEPTH; d++) {
		for(c = 0; c < IN_DEPTH; c++) {
				for(i = 0; i < KERNEL_HEIGHT; i++) {
					for(j = 0; j < KERNEL_WIDTH; j++) {
						data[d][c][i][j] = ((double) rand() / (RAND_MAX));
					}
				}
			}
	}
}


void kernel_transform(float input[3][3], float output[4][4]) {

	//first row
	float temp1a = input[0][0] + input[0][2];

	output[0][0] = input[0][0];
	output[0][1] = (temp1a + input[0][1])/2;
	output[0][2] = (temp1a - input[0][1])/2;
	output[0][3] = input[0][2];

	//second row
	float temp2a = input[0][0] + input[2][0];
	float temp2b = input[1][0] + input[1][2];
	float temp2c = input[2][0] + input[2][2];
	float temp2d = (temp2b + input[1][1])/4;
	float temp2e = (temp2c + input[2][1])/2;
	float temp2f = (temp2b - input[1][1])/4;
	float temp2g = (temp2c - input[2][1])/2;
	float temp2h = output[0][1]/2;
	float temp2i = output[0][2]/2;
	float temp2j = input[0][2] + input[2][2];
	float temp2k = temp2e/2;
	float temp2l = temp2g/2;

	output[1][0] = (temp2a + input[1][0])/2;
	output[1][1] = temp2h + temp2d + temp2k;
	output[1][2] = temp2i + temp2f + temp2l;
	output[1][3] = (temp2j + input[1][2])/2;

	//third row
	output[2][0] = (temp2a - input[1][0])/2;
	output[2][1] = temp2h - temp2d + temp2k;
	output[2][2] = temp2i - temp2f + temp2l;
	output[2][3] = (temp2j - input[1][2])/2;

	//fourth row
	float temp4a = input[2][0] + input[2][2];

	output[3][0] = input[2][0];
	output[3][1] = temp2e;
	output[3][2] = temp2g;
	output[3][3] = input[2][2];
}

/*
 * Matrix([[-(I[2, 0] - I[2, 2]) + I[0, 0] - I[0, 2],    -(I[2, 1] + I[2, 2]) + I[0, 1] + I[0, 2],   -(-I[2, 1] + I[2, 2]) - I[0, 1] + I[0, 2],      -(I[2, 1] - I[2, 3]) + I[0, 1] - I[0, 3]],
 *         [  I[1, 0] - I[1, 2] + I[2, 0] - I[2, 2],       I[1, 1] + I[1, 2] + I[2, 1] + I[2, 2],    -I[1, 1] + I[1, 2] - I[2, 1] + I[2, 2],           I[1, 1] - I[1, 3] + I[2, 1] - I[2, 3]],
 *         [-(I[1, 0] - I[1, 2]) + I[2, 0] - I[2, 2],    -(I[1, 1] + I[1, 2]) + I[2, 1] + I[2, 2],   -(-I[1, 1] + I[1, 2]) - I[2, 1] + I[2, 2],      -(I[1, 1] - I[1, 3]) + I[2, 1] - I[2, 3]],
 *         [-(I[3, 0] - I[3, 2]) + I[1, 0] - I[1, 2],    -(I[3, 1] + I[3, 2]) + I[1, 1] + I[1, 2],   -(-I[3, 1] + I[3, 2]) - I[1, 1] + I[1, 2],      -(I[3, 1] - I[3, 3]) + I[1, 1] - I[1, 3]]])
 */

void input_transform(float input[4][4], float output[4][4]) {

	//first row
	float temp1a = input[2][0] - input[2][2];
	float temp1b = input[2][1] + input[2][2];
	float temp1c = - input[2][1] + input[2][2];
	float temp1d = input[2][1] - input[2][3];

	output[0][0] = - temp1a + input[0][0] - input[0][2];
	output[0][1] = - temp1b + input[0][1] + input[0][2];
	output[0][2] = - temp1c - input[0][1] + input[0][2];
	output[0][3] = - temp1d + input[0][1] - input[0][3];

	//second row
	float temp2a = input[1][0] - input[1][2];
	float temp2b = input[1][1] + input[1][2];
	float temp2c = - input[1][1] + input[1][2];
	float temp2d = input[1][1] - input[1][3];

	output[1][0] = temp2a + temp1a;
	output[1][1] = temp2b + temp1b;
	output[1][2] = temp2c + temp1c;
	output[1][3] = temp2d + temp1d;

	//third row
	output[2][0] = - temp2a + temp1a;
	output[2][1] = - temp2b + temp1b;
	output[2][2] = - temp2c + temp1c;
	output[2][3] = - temp2d + temp1d;

	//fourth row
	output[3][0] = - input[3][0] + input[3][2] + input[1][0] - input[1][2];
	output[3][1] = - input[3][1] - input[3][2] + temp2b;
	output[3][2] = input[3][1] - input[3][2] + temp2c;
	output[3][3] = - input[3][1] + input[3][3] + temp2d;
}

/*
 * Matrix([[O[0, 0] + O[0, 1] + O[0, 2] + O[1, 0] + O[1, 1] + O[1, 2] + O[2, 0] + O[2, 1] + O[2, 2],
 *                                      O[0, 1] - O[0, 2] - O[0, 3] + O[1, 1] - O[1, 2] - O[1, 3] + O[2, 1] - O[2, 2] - O[2, 3]],
 *        [-(O[2, 0] + O[2, 1] + O[2, 2]) - (O[3, 0] + O[3, 1] + O[3, 2]) + O[1, 0] + O[1, 1] + O[1, 2],
 *                                    -(O[2, 1] - O[2, 2] - O[2, 3]) - (O[3, 1] - O[3, 2] - O[3, 3]) + O[1, 1] - O[1, 2] - O[1, 3]]])
 */
void output_transform(float input[4][4], float output[2][2]) {

	//first row
	float temp1a = input[0][1] + input[1][1] + input[2][1];
	float temp1b = input[0][2] + input[1][2];
	float temp1c = input[2][0] + input[2][2];

	output[0][0] = input[0][0] + temp1a + temp1b + input[1][0] + temp1c;
	output[0][1] = temp1a - temp1b - input[0][3] - input[1][3] - input[2][2] - input[2][3];

	//second row
	output[1][0] = - temp1c - input[2][1] - (input[3][0] + input[3][1] + input[3][2]) + input[1][0] + input[1][1] + input[1][2];
	output[1][1] = - (input[2][1] - input[2][2] - input[2][3]) - (input[3][1] - input[3][2] - input[3][3]) + input[1][1] - input[1][2] - input[1][3];
}


void winograd(float input[IN_DEPTH][HLS_INPUT_SIZE][HLS_INPUT_SIZE],
		float kernel[OUT_DEPTH][IN_DEPTH][KERNEL_HEIGHT][KERNEL_WIDTH],
		float output_buffer[OUT_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE]) {

	int i, m, n, k ,j, d;

	//out depth
	for (d = 0; d < OUT_DEPTH; d++) {
		//iterating on channel
		for (i = 0; i < IN_DEPTH; i++) {

			float kernel_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
			kernel_transform(kernel[d][i], kernel_trans);

			struct m_dimension input_offset;
			struct m_dimension output_offset;

			input_offset.height = 0;
			output_offset.height = 0;

			input_offset.width = 0;
			output_offset.width = 0;

			while (input_offset.height <= HLS_INPUT_SIZE - HLS_INPUT_BLOCK_HEIGHT) {

				input_offset.width = 0;
				output_offset.width = 0;

				while (input_offset.width <= HLS_INPUT_SIZE - HLS_INPUT_BLOCK_WIDTH) {

					//duplicate input block
					float partial_input[HLS_INPUT_BLOCK_HEIGHT][HLS_INPUT_BLOCK_WIDTH];
					for (m = 0; m < HLS_INPUT_BLOCK_HEIGHT; m++) {
						for (n = 0; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = input[i][input_offset.height + m][input_offset.width + n];
						}
					}
					float partial_output[HLS_OUTPUT_BLOCK_HEIGHT][HLS_OUTPUT_BLOCK_WIDTH];

					float input_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
					float output_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];

					input_transform(partial_input, input_trans);

					for (k = 0; k < HLS_TRANSFORMED_BLOCK_HEIGHT; k++) {
						for (j = 0; j < HLS_TRANSFORMED_BLOCK_WIDTH; j++) {
							output_trans[k][j] = input_trans[k][j] * kernel_trans[k][j];
						}
					}

					output_transform(output_trans, partial_output);

					int x, y;

					//copying partial output in output position
					for (x = 0; x < HLS_OUTPUT_BLOCK_HEIGHT; x++) {
						for (y = 0; y < HLS_OUTPUT_BLOCK_WIDTH; y++) {
							output_buffer[d][output_offset.height + x][output_offset.width + y] += partial_output[x][y];
						}
					}

					input_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
					output_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
				}

				while (output_offset.width < HLS_OUTPUT_SIZE) {

					int input_width_diff = HLS_INPUT_SIZE - input_offset.width;
					int output_width_diff = HLS_OUTPUT_SIZE - output_offset.width;

					//duplicate input block
					float partial_input[HLS_INPUT_BLOCK_HEIGHT][HLS_INPUT_BLOCK_WIDTH];
					for (m = 0; m < HLS_INPUT_BLOCK_HEIGHT; m++) {
						int n;
						for (n = 0; n < input_width_diff; n++) {
							partial_input[m][n] = input[i][input_offset.height + m][input_offset.width + n];
						}
						for (n; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = 0;
						}
					}
					float partial_output[HLS_OUTPUT_BLOCK_HEIGHT][HLS_OUTPUT_BLOCK_WIDTH];

					float input_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
					float output_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];

					input_transform(partial_input, input_trans);

					for (k = 0; k < HLS_TRANSFORMED_BLOCK_HEIGHT; k++) {
						for (j = 0; j < HLS_TRANSFORMED_BLOCK_WIDTH; j++) {
							output_trans[k][j] = input_trans[k][j] * kernel_trans[k][j];
						}
					}

					output_transform(output_trans, partial_output);

					int x, y;

					//copying partial output in output position
					for (x = 0; x < HLS_OUTPUT_BLOCK_HEIGHT; x++) {
						for (y = 0; y < output_width_diff; y++) {
							output_buffer[d][output_offset.height + x][output_offset.width + y] += partial_output[x][y];
						}
					}

					input_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
					output_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
				}

				input_offset.height += HLS_OUTPUT_BLOCK_HEIGHT;
				output_offset.height += HLS_OUTPUT_BLOCK_HEIGHT;
			}

			while (output_offset.height < HLS_OUTPUT_SIZE) {

				input_offset.width = 0;
				output_offset.width = 0;

				int input_height_diff = HLS_INPUT_SIZE - input_offset.height;
				int output_height_diff = HLS_OUTPUT_SIZE - output_offset.height;

				while (input_offset.width <= HLS_INPUT_SIZE - HLS_INPUT_BLOCK_WIDTH) {

					//duplicate input block
					float partial_input[HLS_INPUT_BLOCK_HEIGHT][HLS_INPUT_BLOCK_WIDTH];
					int m, n;
					for (m = 0; m < input_height_diff; m++) {
						for (n = 0; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = input[i][input_offset.height + m][input_offset.width + n];
						}
					}
					for (m; m < HLS_INPUT_BLOCK_HEIGHT; m++) {
						for (n = 0; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = 0;
						}
					}
					float partial_output[HLS_OUTPUT_BLOCK_HEIGHT][HLS_OUTPUT_BLOCK_WIDTH];


					float input_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
					float output_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];

					input_transform(partial_input, input_trans);


					int k, j;
					for (k = 0; k < HLS_TRANSFORMED_BLOCK_HEIGHT; k++) {
						for (j = 0; j < HLS_TRANSFORMED_BLOCK_WIDTH; j++) {
							output_trans[k][j] = input_trans[k][j] * kernel_trans[k][j];
						}
					}

					output_transform(output_trans, partial_output);


					//copying partial output in output position
					int x, y;
					for (x = 0; x < output_height_diff; x++) {
						for (y = 0; y < HLS_OUTPUT_BLOCK_WIDTH; y++) {
							output_buffer[d][output_offset.height + x][output_offset.width + y] += partial_output[x][y];
						}
					}

					input_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
					output_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
				}

				while (output_offset.width < HLS_OUTPUT_SIZE) {

					int input_width_diff = HLS_INPUT_SIZE - input_offset.width;
					int output_width_diff = HLS_OUTPUT_SIZE - output_offset.width;

					//duplicate input block
					float partial_input[HLS_INPUT_BLOCK_HEIGHT][HLS_INPUT_BLOCK_WIDTH];
					int m, n;
					for (m = 0; m < input_height_diff; m++) {
						for (n = 0; n < input_width_diff; n++) {
							partial_input[m][n] = input[i][input_offset.height + m][input_offset.width + n];
						}
					}
					for (m; m < HLS_INPUT_BLOCK_HEIGHT; m++) {
						for (n; n < HLS_INPUT_BLOCK_WIDTH; n++) {
							partial_input[m][n] = 0;
						}
					}
					float partial_output[HLS_OUTPUT_BLOCK_HEIGHT][HLS_OUTPUT_BLOCK_WIDTH];

					float input_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];
					float output_trans[HLS_TRANSFORMED_BLOCK_HEIGHT][HLS_TRANSFORMED_BLOCK_WIDTH];

					input_transform(partial_input, input_trans);

					for (k = 0; k < HLS_TRANSFORMED_BLOCK_HEIGHT; k++) {
						for (j = 0; j < HLS_TRANSFORMED_BLOCK_WIDTH; j++) {
							output_trans[k][j] = input_trans[k][j] * kernel_trans[k][j];
						}
					}

					output_transform(output_trans, partial_output);

					//copying partial output in output position

					int x,y;

					for (x = 0; x < output_height_diff; x++) {
						for (y = 0; y < output_width_diff; y++) {
							output_buffer[d][output_offset.height + x][output_offset.width + y] += partial_output[x][y];
						}
					}

					input_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
					output_offset.width += HLS_OUTPUT_BLOCK_WIDTH;
				}

				input_offset.height += HLS_OUTPUT_BLOCK_HEIGHT;
				output_offset.height += HLS_OUTPUT_BLOCK_HEIGHT;
			}
		}
	}
}



void naive_3D_convolution_output_no_padding(
		float input[IN_DEPTH][HLS_INPUT_SIZE][HLS_INPUT_SIZE],
		float kernel[OUT_DEPTH][IN_DEPTH][KERNEL_HEIGHT][KERNEL_WIDTH],
		float output[OUT_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE]) {

	size_t o_depth, o_row, o_column, channel, k_row, k_column;

	//iterate over output depth
	for (o_depth = 0; o_depth < OUT_DEPTH; o_depth++) {

		size_t i_row_offset = 0;
		size_t i_column_offset = 0;

		//iterate over output rows
			for(o_row = 0; o_row < HLS_OUTPUT_SIZE; o_row++) {

				//iterate over output columns
				for(o_column = 0; o_column < HLS_OUTPUT_SIZE; o_column++) {

					//init output position to 0
					output[o_depth][o_row][o_column] = 0;

					//iterate over input depth
					for(channel = 0; channel < IN_DEPTH; channel++) {

						//iterate over kernel rows
						for (k_row = 0; k_row < KERNEL_HEIGHT; k_row ++) {

							//iterate over kernel columns
							for(k_column = 0; k_column < KERNEL_WIDTH; k_column ++){
								size_t i_row = i_row_offset + k_row;
								size_t i_column = i_column_offset + k_column;

								output[o_depth][o_row][o_column] += input[channel][i_row][i_column] * kernel[o_depth][channel][k_row][k_column];
							}
						}
					}

					if (i_column_offset + KERNEL_WIDTH == HLS_INPUT_SIZE) {
						i_column_offset = 0;
						i_row_offset += 1;
					} else {
						i_column_offset += 1;
					}
				}
			}
		}
	}


// ------------------------------------------------
// |                INITIALIZE DMAs               |
// ------------------------------------------------
int initDMA(XAxiDma *dma, u32 DeviceId) {
	XAxiDma_Config *CfgPtr;
	int status;

	CfgPtr = XAxiDma_LookupConfig(DeviceId);
	if (!CfgPtr) {
		print("Error looking for AXI DMA config\n\r");
		return XST_FAILURE;
	}
	status = XAxiDma_CfgInitialize(dma, CfgPtr);
	if (status != XST_SUCCESS) {
		print("Error initializing DMA\n\r");
		return XST_FAILURE;
	}
	// Check for scatter gather mode
	if (XAxiDma_HasSg(dma)) {
		print("Error DMA configured in SG mode\n\r");
		return XST_FAILURE;
	}
	// Disable interrupts, we use polling mode
	XAxiDma_IntrDisable(dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

	// Reset DMA
	XAxiDma_Reset(dma);
	while (!XAxiDma_ResetIsDone(dma)) {
	}

	printf("initialization of DMA %d done!\n", (int) DeviceId);
	return XST_SUCCESS;
}

// ------------------------------------------------
// |          FUNCTION THAT TEST CORE             |
// ------------------------------------------------
int main() {
	init_platform();

	int status, i, j;

	static float input[IN_DEPTH][HLS_INPUT_SIZE][HLS_INPUT_SIZE];
	static float kernel[OUT_DEPTH][IN_DEPTH][KERNEL_WIDTH][KERNEL_HEIGHT];

	static float naive_output[OUT_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE];
	static float win_sw_output[OUT_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE];
	static float winograd_output[OUT_DEPTH][HLS_OUTPUT_SIZE][HLS_OUTPUT_SIZE];

		generate_input(input);
		generate_kernel(kernel);

		/*
	    printf("Input: \n");

	    for (int c = 0; c < HLS_DEPTH; c++) {
	        for (int i = 0; i < HLS_INPUT_SIZE; i++) {
	            for (int j = 0; j < HLS_INPUT_SIZE; j++) {
	                printf("%f ", input[c][i][j]);
	                if(j == HLS_INPUT_SIZE - 1) {
	                    printf("\n");
	                }

	            }
	        }
	    }

	    printf("\n");

	    printf("Kernel: \n");

	    for (int c = 0; c < HLS_DEPTH; c++) {
	        for (int i = 0; i < KERNEL_HEIGHT; i++) {
	            for (int j = 0; j < KERNEL_WIDTH; j++) {
	                printf("%f ", kernel[c][i][j]);
	                if(j == KERNEL_WIDTH - 1) {
	                    printf("\n");
	                }

	            }
	        }
	    }

	    printf("\n");
		 */

	// ========== INITIALIZE ENVIRONMENT ==========
	// Disable the cache
	Xil_DCacheDisable();
	Xil_ICacheDisable();

	// Declaration and Initialization of the HW timer
	unsigned int begin_time;
	unsigned int end_time;
	unsigned int calibration;
	double run_time_sw = 0;
	double run_time_hw = 0;

	XTmrCtr timer;

	status = XTmrCtr_Initialize(&timer, XPAR_AXI_TIMER_0_DEVICE_ID);
	if (status != XST_SUCCESS) {
		print("Error: timer setup failed\n");
		return XST_FAILURE;
	}
	XTmrCtr_SetOptions(&timer, 0,
	XTC_AUTO_RELOAD_OPTION | XTC_CASCADE_MODE_OPTION);

	XTmrCtr_Reset(&timer, 0);
	XTmrCtr_Reset(&timer, 1);

	// Calibrate HW timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);
	end_time = XTmrCtr_GetValue(&timer, 0);
	XTmrCtr_Stop(&timer, 0);

	calibration = end_time - begin_time;

	// Declaration and Initialization of the DMAs
	XAxiDma dma_0;

	status = initDMA(&dma_0, 0);
	if (status != XST_SUCCESS) {
		print("\rError: DMA init failed\n");
		return XST_FAILURE;
	}

	// ========== SOFTWARE NAIVE EXECUTION ==========
	printf("\n====================\nSW NAIVE CONVOLUTION:\n====================\n");

	// Start HW timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);

	// Execution on CPU
	naive_3D_convolution_output_no_padding(input, kernel, naive_output);

	// Stop timer
	end_time = XTmrCtr_GetValue(&timer, 0);
	XTmrCtr_Stop(&timer, 0);

	// Compute time
	run_time_sw = (end_time - begin_time - calibration) / TIMER_FACTOR;
	printf("CPU Execution Time: %f \n", run_time_sw);

	// ========== SOFTWARE EXECUTION ==========
		printf("\n====================\nSW WIN CONVOLUTION:\n====================\n");

		// Start HW timer
		XTmrCtr_Start(&timer, 0);
		begin_time = XTmrCtr_GetValue(&timer, 0);

		// Execution on CPU
		winograd(input, kernel, win_sw_output);

		// Stop timer
		end_time = XTmrCtr_GetValue(&timer, 0);
		XTmrCtr_Stop(&timer, 0);

		// Compute time
		run_time_sw = (end_time - begin_time - calibration) / TIMER_FACTOR;
		printf("CPU Execution Time: %f \n", run_time_sw);

		printf("Compare: \n");
			int d;
			for (d = 0; d <  OUT_DEPTH; d++) {
		        //printf("depth: %d\n", d);
					for (i = 0; i < HLS_OUTPUT_SIZE; i++) {
						for (j = 0; j < HLS_OUTPUT_SIZE; j++) {

							/*
				            printf("[%f %f] ", naive_output[d][i][j], winograd_output[d][i][j]);
				            if(j == HLS_OUTPUT_SIZE - 1){
				                printf("\n");
				            }
		*/

							float error = naive_output[d][i][j] - win_sw_output[d][i][j];
							if (error < 0) {
								error = - error;
							}
							if (error > MAX_ERROR) {
								printf("Error\n");
								return 1;
							}
						}
					}
			}


	// ========== HARDWARE EXECUTION ==========
	printf("\n====================\nHW CONVOLUTION:\n====================\n");
	// Start timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);

	// ========== OPEN STREAM TO READ RESULTS ==========
	// Open stream to receive data from the core and start read as soon as the data are available
	XAxiDma_SimpleTransfer(&dma_0, (u32) &(winograd_output[0][0][0]), OUT_DEPTH * HLS_OUTPUT_SIZE * HLS_OUTPUT_SIZE * sizeof(float),
			XAXIDMA_DEVICE_TO_DMA);

	XAxiDma_SimpleTransfer(&dma_0, (u32) &(input[0][0][0]), IN_DEPTH * HLS_INPUT_SIZE * HLS_INPUT_SIZE * sizeof(float),
				XAXIDMA_DMA_TO_DEVICE);
		// Wait until data of this transfer are received.
		while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE))
			;

	// ========== SEND MATRIXES ==========
	// Send points to the core
	XAxiDma_SimpleTransfer(&dma_0, (u32) &(kernel[0][0][0][0]), OUT_DEPTH * IN_DEPTH * KERNEL_WIDTH * KERNEL_HEIGHT * sizeof(float),
			XAXIDMA_DMA_TO_DEVICE);
	// Wait until data of this transfer are received.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE))
		;
	// ========== WAIT RESULTS ==========
	// Wait until all data of output are read from the core.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DEVICE_TO_DMA))
				;

	// Stop timer
	end_time = XTmrCtr_GetValue(&timer, 0);

	XTmrCtr_Stop(&timer, 0);

	run_time_hw = (end_time - begin_time - calibration) / TIMER_FACTOR;
	printf("FPGA Execution Time: %f \n", run_time_hw);

	printf("Compare: \n");
	int e;
	for (e = 0; e <  OUT_DEPTH; e++) {
        //printf("depth: %d\n", d);
			for (i = 0; i < HLS_OUTPUT_SIZE; i++) {
				for (j = 0; j < HLS_OUTPUT_SIZE; j++) {

					/*
		            printf("[%f %f] ", naive_output[d][i][j], winograd_output[d][i][j]);
		            if(j == HLS_OUTPUT_SIZE - 1){
		                printf("\n");
		            }
*/

					float error = naive_output[e][i][j] - winograd_output[e][i][j];
					if (error < 0) {
						error = - error;
					}
					if (error > MAX_ERROR) {
						printf("Error\n");
						return 1;
					}
				}
			}
	}
			printf("Compare OK!\n");
			cleanup_platform();


			return 0;
}

