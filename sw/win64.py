from pynq.drivers.dma import DMA
import numpy as np
from random import randint
import time
from pynq.drivers.dma import ffi
from pynq import Overlay

def winograd(input_tensor, kernel_tensor):
    ol = Overlay("/home/xilinx/win64.bit")
    ol.download()

    concat_tensor = np.concatenate([input_tensor, kernel_tensor])
    
    in_len = int(3 * 3 * 3 * 64 + 64 * 64 * 3)
    out_len = int(62 * 62 * 64)
    
    PStoPL = DMA(ol.ip_dict["SEG_axi_dma_0_Reg"][0], direction=0) 
    PLtoPS = DMA(ol.ip_dict["SEG_axi_dma_0_Reg"][0], direction=1) 
    PStoPL.configure()
    PLtoPS.configure()

    in_size=int(in_len * 4) 
    out_size = int(out_len * 4)

    PStoPL.create_buf(in_size)
    PLtoPS.create_buf(out_size)

    in_buf = ffi.buffer(PStoPL.buf, in_size)
    in_buf_view = np.frombuffer(in_buf, np.float32, -1)
    in_buf_view[:] = concat_tensor

    out_buf = ffi.buffer(PLtoPS.buf, out_size)
    out_buf_view = np.frombuffer(out_buf, np.float32, -1)

    PStoPL.transfer(in_size, direction=0)
    PLtoPS.transfer(out_size, direction=1) 
    PStoPL.wait()  
    PLtoPS.wait()
    
    result = np.fromstring(out_buf_view, dtype=np.float32)

    PStoPL.free_buf()
    PLtoPS.free_buf()
    return result

in_len = int(64 * 64 * 3)
ker_len = int(3 * 3 * 3 * 64)

in_vector = np.random.uniform(low=0, high=1, size=(in_len,))
ker_vector = np.random.uniform(low=0, high=1, size=(ker_len,))
out = winograd(in_vector, ker_vector)
print('First 10 output points..')
print(out[:10])
